# Valign

Adds classes to align elements vertically.

## Installing with bower:

Run `bower i --save valign=git@bitbucket.org:rubenestevao/valign.git` in your terminal
